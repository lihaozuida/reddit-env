-- phpMyAdmin SQL Dump
-- version 4.3.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 12, 2015 at 12:32 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ranker`
--
create database if not exists ranker;
use ranker;

-- --------------------------------------------------------

--
-- Table structure for table `ranker_content`
--

CREATE TABLE IF NOT EXISTS `ranker_content` (
  `content_id` int(11) unsigned NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` varchar(512) NOT NULL,
  `url` varchar(512) NOT NULL,
  `image_url` varchar(512) NOT NULL,
  `content_md5` char(32) NOT NULL,
  `create_time` datetime NOT NULL,
  `up` int(11) unsigned NOT NULL,
  `down` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 COMMENT='main content of ranker';

--
-- Dumping data for table `ranker_content`
--

INSERT INTO `ranker_content` (`content_id`, `title`, `description`, `url`, `image_url`, `content_md5`, `create_time`, `up`, `down`) VALUES
(1, '小米五周年，从几张图看看中国创业故事', '“五年前的今天，2010年4月6日，北京中关村保福寺桥银谷大厦807室，14个人，一起喝了碗小米粥，一家小公司就开张了。', 'http://36kr.com/p/531485.html', 'http://e.36krcnd.com/nil_class/f561b9f9-8b0b-448d-a5ea-f37af821e4d8.JPG!slider', 'cdf0c0edbfb93ef93143c0ef0bb58ff9', '2015-04-11 00:00:00', 2, 0),
(2, '随手拥有明星同款，4月第1周不可错过的10款新产品|NEXT Weekly', '即使假期还剩最后一天，也要刷新更酷的生活方式。', 'http://36kr.com/p/531484.html', 'http://b.36krcnd.com/nil_class/092cee32-23bd-4d29-9c21-bb7f13e52c5d.jpg!slider', '2b7614314b4fa6c3576b6aabeaaebbe9', '0000-00-00 00:00:00', 3, 0),
(3, '【氪TV视频】找出自己的极限，能测乳酸阀的 BSXinsight', '以往的乳酸阀测试都必须验血,现在带个护腿就好了', 'http://36kr.com/p/531483.html', 'http://a.36krcnd.com/nil_class/fa12ab36-c9c8-4d2b-a2cf-93e20bad5d69.jpg!slider', '55094b9663222817c476b68da5e2f45b', '0000-00-00 00:00:00', 0, 0),
(4, '9点1氪：苹果表示，不懂Apple Watch你就别买了', 'Apple Watch不支持直接到店购买；Uber新任CSO是从Facebook挖来的美国联邦检察官；多次跳票的“氧OS”开放下载。', 'http://36kr.com/p/531482.html', 'http://e.36krcnd.com/nil_class/ca1dd69b-6e0f-4bc3-9461-b30ccf4788b1.jpg!slider', '866afacfed1f51c32340e8018005b69f', '0000-00-00 00:00:00', 0, 0),
(5, '【氪TV视频】开车时忍不住玩手机？那么你需要RayGo', '用它来处理必要的事务，可不是鼓励你边开车边玩手机！', 'http://36kr.com/p/531481.html', 'http://e.36krcnd.com/nil_class/286e27ec-d06b-4d76-ad0a-3c2763d54ecb.jpg!slider', '7285c0323ec03b8a13e5187ee9b0180f', '0000-00-00 00:00:00', 0, 0),
(6, '从社交到音乐，Rithm 是这么切的', '其实，我怎么感觉这在国内其实好像不是什么新鲜玩法了。', 'http://36kr.com/p/531480.html', 'http://d.36krcnd.com/nil_class/8591c4b4-706b-420b-a27c-adc4c5d4ecf2.png!slider', 'ca9675e146c7df761cca2a6500c35f02', '0000-00-00 00:00:00', 0, 0),
(7, '微软的技术院士说开源Windows绝对有可能', 'Windows无疑是微软帝国的根基，这个无所不在的操作系统与其Office办公套件是软件巨头的收入基石。', 'http://36kr.com/p/531479.html', 'http://d.36krcnd.com/nil_class/0f11b83c-3052-42d1-8649-3cf0b7e61299.jpg!slider', '2614c0f3170074814d9fbc699684d0e7', '0000-00-00 00:00:00', 0, 0),
(8, '秒视 Kevin ：我喜欢有灵魂的产品，这样才不会太无聊', '「当你不显现于产品的时候，产品亦不会显现于用户」。', 'http://36kr.com/p/531478.html', 'http://b.36krcnd.com/nil_class/f534e20c-ec46-4c9e-b911-ceff6b197320.jpg!slider', '9a879ded88f3a7f0b348e833f2a8c507', '0000-00-00 00:00:00', 0, 0),
(9, '【氪TV视频】让 iPhone 凭空转圈的凶残应用 Cycloramic', '说不定你见过旧版，那iPhone6这么圆润，要怎么转呢？', 'http://36kr.com/p/531477.html', 'http://b.36krcnd.com/nil_class/ae861548-1c97-480c-b2ac-43c08e4d09ff.jpg!slider', '07757f00d6625180b98520053c5ec7c4', '0000-00-00 00:00:00', 0, 0),
(10, '阿里成立智能生活事业部，智能家居领域初现三足鼎立态势', '天猫电器城+阿里智能云+淘宝众筹＝智能生活事业部。', 'http://36kr.com/p/531410.html', 'http://b.36krcnd.com/nil_class/870fd362-05ba-457d-b3df-ca39d966bb45.jpg!slider', '307a97751c9c47700a21c9a136c86d2d', '0000-00-00 00:00:00', 0, 0),
(11, '上手全世界只有6台的Vive 头显，是一种什么样的体验？', '买买买！', 'http://36kr.com/p/531476.html', 'http://b.36krcnd.com/nil_class/de121e4b-389c-4402-9b12-b9d33ac98008.png!slider', 'dd72e063bf39b2aebc8af0db0145b016', '0000-00-00 00:00:00', 0, 0),
(12, '越过2014年新硬件的死亡谷，谁活着爬出来了？', '从前 现在 过去了再不来', 'http://36kr.com/p/531409.html', 'http://a.36krcnd.com/nil_class/5369cc11-7614-47a0-a750-7869a9c2b685.jpg!slider', '44a19299f5d95b5b50c0d6d7b2b791d0', '0000-00-00 00:00:00', 0, 0),
(13, '9点1氪：Apple Watch将于4月10日下午3点开放预售，配套的AppleCare+服务售价59-999美元', 'Apple Watch将于 4月10日下午3点01分开放预购；2014年，全球75%的社交广告通过Facebook传播。', 'http://36kr.com/p/531475.html', 'http://e.36krcnd.com/nil_class/3bf04b98-80f7-4c2b-a1f9-c8bad4287131.png!slider', 'b813037743e60f299f6f42875b7f7260', '0000-00-00 00:00:00', 0, 0),
(14, '继“一键叫船”之后，Uber在杭州又推出了“打飞机”业务', '终于可以公然打飞机了……', 'http://36kr.com/p/531408.html', 'http://e.36krcnd.com/nil_class/2e0dc42c-cc98-495b-a59d-147aab9a8bdf.jpg!slider', '61b4991c57ad9d71dc52150083dceddf', '0000-00-00 00:00:00', 0, 0),
(15, '你以为互联网让你更聪明，那是因为有了搜索引擎', '成员间共享集体需要的信息在社会系统中很常见，比方说一个宿舍里有人会记得什么时候该付账，有人会记得送外卖的电话等等，这种所谓的交互记忆系统有助于大家的生活变得更加简单。', 'http://36kr.com/p/531474.html', 'http://a.36krcnd.com/nil_class/4a2499fa-bbca-42aa-ae2a-0d4da4c1b7d4.jpg!slider', '6487ccdd3df9acbab3e768de662c0cde', '0000-00-00 00:00:00', 0, 0),
(16, '开放教育资源平台Lumen Learning获250万美元种子融资', '这家公司让原本要150美元的课程资料变成了10美元。', 'http://36kr.com/p/531407.html', 'http://c.36krcnd.com/nil_class/b63d16ae-f9ae-4629-a716-c7b5cc1160b1.jpg!slider', '42aa3c36c8602a78400a9af73525abf4', '0000-00-00 00:00:00', 0, 0),
(17, '“醒醒”的晨起社交：垂直化场景上的工具社交试水与转型', '“我有起床气，怎么破”', 'http://36kr.com/p/531423.html', 'http://c.36krcnd.com/nil_class/e9e03884-54de-4ce0-a9b0-349608e13ea0.jpg!slider', '2be12c9da820144b997a903ab30c50dd', '0000-00-00 00:00:00', 0, 0),
(18, '【氪TV视频】4月第1周最棒的5个APP在这里了——「下一个应用」', '本周，我们发现了这几个最酷的APP！', 'http://36kr.com/p/531473.html', 'http://d.36krcnd.com/nil_class/f1ba9a1a-07b9-4b63-bf5b-f64320eb5b85.jpg!slider', '459f46b37a87d93c4244ad347d9c7738', '0000-00-00 00:00:00', 0, 0),
(19, '对许多创业公司来说，最大的压力就是“迭代太慢”', '昨天采访知乎的CEO周源，快收尾时闲聊问到:现在知乎最大的压力是什么?', 'http://36kr.com/p/531406.html', 'http://c.36krcnd.com/nil_class/e73cffcd-27a9-4974-9f51-7b390104e5d3.jpg!slider', '9bfbe3603b971d6170c6fc6f5769e80d', '0000-00-00 00:00:00', 0, 0),
(20, '趣分期宣布完成D轮“巨额”融资，准备冲击十亿美元俱乐部', '这又是抱上了谁的大腿？', 'http://36kr.com/p/531441.html', 'http://c.36krcnd.com/nil_class/20198699-90c1-4242-9bce-7b766f318aed.jpeg!slider', '3ec3478b5ed022a51837169e4e4d8f07', '0000-00-00 00:00:00', 0, 0),
(21, '开发最小化可行产品（MVP）时，除了“最小化”之外，更要关注“可行”', '开发MVP注意避免走入误区', 'http://36kr.com/p/531457.html', 'http://b.36krcnd.com/nil_class/f28e89f3-65c1-4446-8856-b2aa721eba5d.jpeg!slider', 'a6e35ac5535fc0ee0138586512abd2ee', '0000-00-00 00:00:00', 0, 0),
(22, 'Benedict Evans :消息应用与移动平台', '原文是Benedict Evans对未来移动平台和消息应用走势的宏大探索。', 'http://36kr.com/p/531405.html', 'http://e.36krcnd.com/nil_class/86a6faff-63db-4b42-91c2-34a2169bc1ff.jpg!slider', 'acc8e66e1ab1d06413add82fcafb7560', '0000-00-00 00:00:00', 0, 0),
(23, '中高端二手车买卖怎么做？看看C2B平台“又一车”是怎么和车后连锁“美车堂”1+1>2的', '要高端，要精致。', 'http://36kr.com/p/531440.html', 'http://c.36krcnd.com/nil_class/4710e8a5-cf16-474f-a0bd-3d0f3283c027.jpeg!slider', '349a8e33f7ba007e9149f53b21632ac0', '0000-00-00 00:00:00', 0, 0),
(24, '36氪每日投融资笔记：趣分期宣布完成D轮“巨额”融资，准备冲击十亿美元俱乐部', '从A轮到D轮，只用了1年。', 'http://36kr.com/p/531456.html', 'http://d.36krcnd.com/nil_class/bca1c45c-61cf-4616-9881-33199a5712b6.jpeg!slider', '738dd0c272c3db15b91ff7a679802282', '0000-00-00 00:00:00', 0, 0),
(25, '【氪TV视频】心态很重要，能监测压力水平的 Emvio', '只有管理好压力，才能舒舒服服地继续被社会压榨。', 'http://36kr.com/p/531404.html', 'http://e.36krcnd.com/nil_class/2b5fc23d-0e7a-4a0e-a395-45ed6282e183.png!slider', 'fab2535bf2e983ac672cdd5504ed0aae', '0000-00-00 00:00:00', 0, 0),
(26, 'Apigee Link：帮硬件厂商打通物联网API接口，让设备在云端沟通互联', '界面结合、设备互联后，真正的物联网时代才算到来。', 'http://36kr.com/p/531439.html', 'http://c.36krcnd.com/nil_class/c2f5eb60-9f7d-4e4d-8523-f6b44131c979.jpg!slider', '8b6dbda5033e0314ec38233740650166', '0000-00-00 00:00:00', 0, 0),
(27, '“滴滴”叫大麻，看准医用合法交易市场', '目测实用，却没法亲测……', 'http://36kr.com/p/531455.html', 'http://d.36krcnd.com/nil_class/73d34083-ce87-4d6d-b826-d287a989cdb2.png!slider', '25762376ab6aee756f1c6d9da912ab23', '0000-00-00 00:00:00', 0, 0),
(28, '团购鼻祖Groupon试做B2C折扣电商，也要成为“做特卖的网站”？', '类似唯品会，但更多是中小品牌。', 'http://36kr.com/p/531403.html', 'http://a.36krcnd.com/nil_class/16e2706a-ce3c-4d58-aed6-d5ebe5774195.jpg!slider', 'f8c3bf4c395f2213a0909b37f886865d', '0000-00-00 00:00:00', 0, 0),
(29, '集成Apple Pay功能，发力移动端票务市场，SeatGeek获6200万美元C轮融资', 'SeatGeek是一个创建于2009年的在线票务搜索比价引擎，用户可以通过Web版或移动应用订到自己需要的门票。', 'http://36kr.com/p/531438.html', 'http://a.36krcnd.com/nil_class/0fbfb739-bd6e-4f2d-aa54-98ec5d7745de.jpg!slider', 'a5eae7dfce6069e0d89ef8342e7860c8', '0000-00-00 00:00:00', 0, 0),
(30, '帮你用移动设备分析球队比赛的每一帧，Hudl获7250万美元A轮融资', '增加了社交性后，它变成了一个平台，还为体育爱好者提供了一个相对垂直专业的内容展示区。', 'http://36kr.com/p/531454.html', 'http://e.36krcnd.com/nil_class/9a1306b9-7f91-4e1c-b14d-2ec3d09fd22e.png!slider', '5aeec39e1ba0948f048e26f4d113f2fd', '0000-00-00 00:00:00', 0, 0),
(31, 'Kickstarter推出问答社区Campus，想在平台沉淀更多众筹经验', '已经有一些创建者开始在上面展开提问，像如何设定众筹的金额。', 'http://36kr.com/p/531402.html', 'http://e.36krcnd.com/nil_class/e27d60ba-7a13-42e5-8610-125c26de9843.png!slider', '5aa31586a7d94730a7ad96451a52443f', '0000-00-00 00:00:00', 0, 0),
(32, '饱受安全非议的Uber，从Facebook挖来了首席安全官', 'Uber的“安全之殇”...', 'http://36kr.com/p/531437.html', 'http://d.36krcnd.com/nil_class/5fee082c-7a3d-4146-bd49-549bb2fff80e.png!slider', 'a37bfac69fc84980ae0b3457d924687b', '0000-00-00 00:00:00', 0, 0),
(33, '如果服务同信息一样存在长尾，Magic可能就是下一个Google', '上世纪末，互联网上信息的激增催生了Google这样的搜索引擎巨头，在很长的时间里替代上一代门户网站成了许多人的网上入口。', 'http://36kr.com/p/531453.html', 'http://b.36krcnd.com/nil_class/3602a612-02fe-403b-b9aa-651b31899c2e.jpg!slider', 'f239f399dc2536a62e48bc22b7445e8f', '0000-00-00 00:00:00', 0, 0),
(34, '任天堂 N64 和 DS 游戏，正式登陆Wii U 的 VC 平台', '这才是游戏嘛，现在的年轻人玩什么撸啊撸。', 'http://36kr.com/p/531401.html', 'http://a.36krcnd.com/nil_class/6ade583a-6228-4c90-a251-a75e7f079906.png!slider', '58071bf822550bc9745e66247757a709', '0000-00-00 00:00:00', 0, 0),
(35, '挑战生活分类信息鼻祖Craigslist，闲置交易应用VarageSale要改革本地二手交易市场', '真是搞不懂为什么又老又丑的Craigslist能垄断那么久。', 'http://36kr.com/p/531436.html', 'http://c.36krcnd.com/nil_class/15b6a602-6bef-4fff-983a-7ee5eba18097.jpg!slider', '7c4561d6a226827d23b2505d06344ecb', '0000-00-00 00:00:00', 0, 0),
(36, '【WISE Talk】解救除了焦虑、还是焦虑的传统餐饮行业，互联网人的想法在这里', '本来，我并不想过分强调“焦虑”这个词，但是看了茶水间主任写他和蔡林记老板的对话之后，我觉得这个词实在恰如其分。', 'http://36kr.com/p/531452.html', 'http://c.36krcnd.com/nil_class/cd728934-1299-4209-b42d-7448653e1b2d.jpg!slider', '23783216e53082533fe7fbdd9f09e3ea', '0000-00-00 00:00:00', 0, 0),
(37, 'Facebook推出协作视频应用Riff，一起来用视频盖楼吧', '和好友一起盖楼，录一段温馨的祝福视频给TA吧。', 'http://36kr.com/p/531400.html', 'http://e.36krcnd.com/nil_class/8d00929a-d922-470f-96eb-bbcca1ceabf7.gif!slider', '80e1c13125d92afe65ddeb1b32f97d96', '0000-00-00 00:00:00', 0, 0),
(38, '不要用互联网思维“调戏”自行车文化', '不忘初心，方得始终。慢，即是快。', 'http://36kr.com/p/531435.html', 'http://b.36krcnd.com/nil_class/41c2184f-54b8-44b9-9a15-43f0b40756e6.jpg!slider', '679186ab0f87ad9a2050f4ff041598b4', '0000-00-00 00:00:00', 0, 0),
(39, '这只“熊”有点特别，上门推拿O2O领域的又一新玩家“熊猫拿拿”', '一只身世特殊的推拿“熊”', 'http://36kr.com/p/531451.html', 'http://a.36krcnd.com/nil_class/0cee0a13-be49-4784-a9bd-ec6a9b1d3f3b.png!slider', '6c85ec67a79b161a95e7283d7dab5832', '0000-00-00 00:00:00', 0, 0),
(40, '健康管理初创团队Noom完成B轮融资，增大“个性化健康训练服务”的研发投入', 'Noom拥有两种不同服务，Noom Coach主要是帮助用户建立良好运动生活习惯，达到减肥效果；Noom Health则是健康服务平台，对接供应商。', 'http://36kr.com/p/531399.html', 'http://d.36krcnd.com/nil_class/dc7819e5-c51e-446d-a075-0f3bdf850cd7.png!slider', 'd098b03f4a5c7d1da783b690fb1b0c71', '0000-00-00 00:00:00', 0, 0),
(41, '为保护用户隐私，Snapchat禁止第三方接入，并发布政府数据请求报告', '保护隐私，赢得信任，才是长久之计——这点国内的互联网巨头应该好好学学。', 'http://36kr.com/p/531434.html', 'http://e.36krcnd.com/nil_class/315738eb-4c00-49f2-87fb-4aade487f057.jpg!slider', '4d20f4c1bea4bc4548efb2d600783319', '0000-00-00 00:00:00', 0, 0),
(42, '为什么说苹果表一定会死', '库克，你怎么看？', 'http://36kr.com/p/531427.html', 'http://b.36krcnd.com/nil_class/583f3b46-7028-4ab0-a559-da0a382c8633.jpg!slider', '3d3d778b7206015fd811e2cab32eae7e', '0000-00-00 00:00:00', 0, 0),
(43, '支付宝和华米合作开发可穿戴设备支付，双方瞄准的都是更大的用户使用场景', '可穿戴支付这是一个切入点，但是双方都有更大的野心，一个相连接更多App，一个想让用户随时随地支付', 'http://36kr.com/p/531398.html', 'http://a.36krcnd.com/nil_class/ac853569-b104-4e6f-aefa-8f24a8d58d77.JPG!slider', '489071dc76fd90fd0719afe31ab5e4ec', '0000-00-00 00:00:00', 0, 0),
(44, '在Uber和公交之间找定位，拼车服务Via获B轮融资2700万美金', '不管是运人还是运物，Uber模式在全世界范围都刺激了交通方式的多元化，每一种细分的方式都不乏竞争，拼车是其中一种。', 'http://36kr.com/p/531433.html', 'http://c.36krcnd.com/nil_class/c4607275-6e43-46d0-957d-2acef157b8a8.png!slider', 'ddf521b5d5ab6aef1fbc2529da894bd2', '0000-00-00 00:00:00', 0, 0),
(45, '帮主办方掌握流动的用户信息，“活动行”从细节拼出一套Event标准化流程', '春天来了，找个人多有活动的地方玩耍吧。', 'http://36kr.com/p/531450.html', 'http://c.36krcnd.com/nil_class/b6a22ee4-b945-4560-b23f-57f8fac9a261.png!slider', 'a986ffc4ab762d573553cfbc514a6cdf', '0000-00-00 00:00:00', 0, 0),
(46, '未来iPhone怎么解锁，Apple说“拍拍拍，根本停不下来”', '对不起，未检测到面部信息......', 'http://36kr.com/p/531397.html', 'http://c.36krcnd.com/nil_class/079ea0de-e068-4bcb-b623-c63abd0f110f.jpg!slider', '587e9b7aa2b5e0acf9ed488f7b715b04', '0000-00-00 00:00:00', 0, 0),
(47, '云游戏先驱OnLive死了，吃下它专利的PlayStation Now会走多远呢？', '云游戏市场还是一块没有充分开发的荒漠。', 'http://36kr.com/p/531432.html', 'http://e.36krcnd.com/nil_class/ad70645c-c8e2-459b-8a2b-8270d8b4b6d2.jpg!slider', '41a21c291a8ac88509c9ea1121dfb82a', '0000-00-00 00:00:00', 0, 0),
(48, 'Adobe 推出教育产品 Slate，以Storytelling的形式呈现知识', '教育是块大市场，大家都来啃啊啃啊。', 'http://36kr.com/p/531449.html', 'http://d.36krcnd.com/nil_class/345ddabd-b6ec-4c3a-ba93-168d9de7b084.png!slider', '6f0228ea1df12933421509ed94f0c85d', '0000-00-00 00:00:00', 0, 0),
(49, 'Google 又为 Chromebook 准备了教育新套餐，拿下了北美市场后，下一站是亚洲', '三款新品，要么是黑科技，要么是价格低，让不让别人玩了。', 'http://36kr.com/p/531396.html', 'http://d.36krcnd.com/nil_class/56968206-edaf-4eea-b022-093d2a96b87e.jpg!slider', 'e25135dae23ff130f612cbcaa6594c7a', '0000-00-00 00:00:00', 0, 0),
(50, '微软推出移动端文件扫描应用Office Lens，试图将产品引入Windows系统之外的更多设备', '文中有下载链接，好奇星人试试先。', 'http://36kr.com/p/531431.html', 'http://e.36krcnd.com/nil_class/405f9994-7c36-4a52-bd81-1d70d144b3ee.png!slider', '57da12f5a09dc7e2bae43d43f54967bb', '0000-00-00 00:00:00', 0, 0),
(51, 'Benedict Evans是怎么看待小米的？', '硅谷投资人眼中的小米', 'http://36kr.com/p/531444.html', 'http://a.36krcnd.com/nil_class/12cf3588-1388-4bf8-aa56-f0ed5b924541.png!slider', 'e8622db76a16125d1fdb3c68dbcc4ae8', '0000-00-00 00:00:00', 0, 0),
(52, 'Meerkat的春天太短暂', '从西南偏南到拿到1200万美金的融资，Meerkat的增长曲线达到一个峰值就开始往下跌了，中间发生了什么，', 'http://36kr.com/p/531395.html', 'http://e.36krcnd.com/nil_class/05726271-fa65-4c70-b1bf-4506a9ff0371.jpg!slider', '03275b0ee2d037e9861bdb2c88d80842', '0000-00-00 00:00:00', 0, 0),
(53, '给老年人的到家服务，Honor获2000万美金A轮融资', '通常老年人对新兴的科技不感冒，而程序猿们总想着如何去改变年轻一代的生活方式。', 'http://36kr.com/p/531430.html', 'http://d.36krcnd.com/nil_class/9de71749-7459-4d59-92c6-738a10906dee.jpg!slider', '7008e3cd6f66ec09a2bfcfc0498f3e14', '0000-00-00 00:00:00', 0, 0),
(54, '互联网证券是风口上的猪吗？（连载一）', '此文期望展示给读者整个国内证券行业的大环境及发展预期，给予希望了解这个市场的人一些谈资。', 'http://36kr.com/p/531448.html', 'http://b.36krcnd.com/nil_class/23b46ca2-fb07-4862-a7ca-f6d3cbff6912.png!slider', 'a2211eb8d8396976ff8426bd9812b544', '0000-00-00 00:00:00', 0, 0),
(55, '一向高冷的Chanel放下身段，宣布明年进军电商了', '一开始听说网上卖奢侈品，它是拒绝的。', 'http://36kr.com/p/531394.html', 'http://d.36krcnd.com/nil_class/76fe6c08-b49f-4aed-9702-6b1396ea6799.png!slider', 'c537a8c1161b278af9f2e7608caf25e0', '0000-00-00 00:00:00', 0, 0),
(56, 'iPhone 杀手：Apple Watch 秘史', '关于 Apple Watch 背后的种种故事，最深刻的揭晓。', 'http://36kr.com/p/531429.html', 'http://a.36krcnd.com/nil_class/1520e20d-8219-426f-8637-c4df66cc1256.jpg!slider', '22c056635a901ae9509b7bfb4ce6dda1', '0000-00-00 00:00:00', 0, 0),
(57, '快的推出“一号快车”，将了谁的军', '接下来，是一场史诗般的战役。', 'http://36kr.com/p/531447.html', 'http://e.36krcnd.com/nil_class/b32df331-1285-4f3b-bb63-dc9e812416fa.jpg!slider', '0c6329dab768f56c31cb22c607fff129', '0000-00-00 00:00:00', 0, 0),
(58, '8点1氪：Apple TV希望内容商为流媒体服务买单，GoDaddy上市首日股价涨幅达30%', '基于ARM的Atmel新款芯片，将电池寿命延长数十年。Facebook推出新应用Riff，把大家拍的小视频串起来。', 'http://36kr.com/p/531393.html', 'http://d.36krcnd.com/nil_class/dbf4cbdc-1345-480e-8c67-c57a9077835c.jpeg!slider', '672050b2bcbf9b6afd222d4c3220d1ff', '0000-00-00 00:00:00', 0, 0),
(59, '8点1氪：欧盟委员会向苹果、Google齐开炮，继续反垄断', 'Comcast超越Google Fiber，要为家庭提供2 Gbps的网络服务；Google Chrome和火狐浏览器将不再信任CNNIC授权的电子证书。', 'http://36kr.com/p/531428.html', 'http://e.36krcnd.com/nil_class/cbbb1a5e-9f4d-4708-9da0-a16ff5a3c235.jpg!slider', '202ddf0f02a56ce2abf270c94c0e98dc', '0000-00-00 00:00:00', 0, 0),
(60, '欧盟监管升温，直指Facebook、苹果、谷歌', '三匹来自美国的狼，在欧洲被筛成了狗。', 'http://36kr.com/p/531446.html', 'http://b.36krcnd.com/nil_class/c15877cc-3964-455c-9f56-1f8348d94053.jpg!slider', '98e2a3835b344164bb0ce40bc702b3cc', '0000-00-00 00:00:00', 0, 0),
(61, '【WISE Talk】一碗热干面背后的“互联网思维”，那些被我们轻视的“传统企业”是如何居安思危的\r\n', '“每天餐馆坐满了人，但我真的不知道我的用户在哪里.', 'http://36kr.com/p/531392.html', 'http://d.36krcnd.com/nil_class/2a15792a-ef24-4841-9201-39ccae3ade17.jpg!slider', 'd8b304c93c220e4fc377b3ca00ad9492', '0000-00-00 00:00:00', 0, 0),
(62, '为什么 Jay Z 声势浩荡的 Tidal，一定搞不起来', 'Ben Lefsetz 一针见血的观察，和超凶超狠的评价......', 'http://36kr.com/p/531426.html', 'http://b.36krcnd.com/nil_class/499ebbbc-bf32-47c2-8b33-c03e767e9388.jpg!slider', '6a9c7584e22f6a080ba1397e4ab1834e', '0000-00-00 00:00:00', 0, 0),
(63, '“拍立送”快递服务Shyp又融资5000万美元，估值2.5亿美元', '拍一张你要送出物品的照片，冷冰冰的物流突然有了暖意。', 'http://36kr.com/p/531445.html', 'http://e.36krcnd.com/nil_class/6eefe70a-2636-46b4-b759-cf5532044347.jpg!slider', '1c4e2e01a3a6e380211670ea24aab7ee', '0000-00-00 00:00:00', 0, 0),
(64, '支付宝的愚人节畅想：社交金融系统“到位”', '社交+金融，这不会是未来的土豪约X神器吧？', 'http://36kr.com/p/531391.html', 'http://b.36krcnd.com/nil_class/6e1d8d26-5350-4898-be7c-bed8dfa09478.jpg!slider', '0e6bb9a741a1b1bc691958cafb4eea8b', '0000-00-00 00:00:00', 0, 0),
(65, 'Google调整研发策略：创意可以无限，但是时间有限', '在投入巨大的长期研究项目受挫并遭受投资者质疑之后，搜索巨头Google准备调整研发策略，转向更精益、更快捷的创新方式。', 'http://36kr.com/p/531425.html', 'http://e.36krcnd.com/nil_class/2e53bdc1-a192-4bf2-bb5b-c56feec5cd59.gif!slider', '59c199d6055c04e24d0531c801913a50', '0000-00-00 00:00:00', 0, 0),
(66, '【氪TV视频】任性的iKlips，就是要用U盘在iOS设备间传文件', '你可以说我笨，不过我文件已经传完了。', 'http://36kr.com/p/531443.html', 'http://d.36krcnd.com/nil_class/cbc0fcef-b0c4-4cb8-88bf-12fb180c937c.png!slider', '3e7c62ae9988fadb34d5de1c647fb74c', '0000-00-00 00:00:00', 0, 0),
(67, '36氪每日投融资笔记：熟食O2O老枝花卤融资千万元，王中磊朱拥华等投资', '王中磊之前还投了一个叫个鸭子，娱乐圈的投资逻辑我不懂...', 'http://36kr.com/p/531390.html', 'http://c.36krcnd.com/nil_class/8932334b-515c-439b-a0d2-2e2200d6c9dd.png!slider', '7a847732a51184392a5e7dd71dcfbc87', '0000-00-00 00:00:00', 0, 0),
(68, '36氪每日投融资笔记：礼物说”完成3000万美金B轮融资，估值突破2亿美金', '又一个90后创业者的创业神话。', 'http://36kr.com/p/531424.html', 'http://d.36krcnd.com/nil_class/8f47e978-cf15-4901-b46e-598d8242a311.jpg!slider', '62b999d997ba5262c1c9d3846a5b7934', '0000-00-00 00:00:00', 0, 0),
(69, '信誓旦旦说过不会做广告的 Tinder，昨天上线了其第一个广告', '看到这则资讯，想起今天周五，是时候打开封存已久的 Tinder 了。', 'http://36kr.com/p/531442.html', 'http://a.36krcnd.com/nil_class/702db5cb-4959-4f66-aecb-2a8231707bd6.jpg!slider', 'bfe5d8cf3740feba4e8b70c11ac71eab', '0000-00-00 00:00:00', 0, 0),
(70, '对冲基金Coatue买入滴滴快的6亿美元股份，快的高管或套现离场', '来来来，卖了这些股份，你的估值就是Uber的1/5了。', 'http://36kr.com/p/531389.html', 'http://b.36krcnd.com/nil_class/c99a030b-786d-424c-b093-02ae1478c966.jpg!slider', 'ab2265b472ccdc7f66df6e50e50f2e46', '0000-00-00 00:00:00', 0, 0),
(71, '互联网已经诞生四十几年了，为什么停车市场还这么落后？', '罐头是在1810年发明出来的，可开罐器却在1858年才被发明出来。', 'http://36kr.com/p/531422.html', 'http://d.36krcnd.com/nil_class/c860aea8-92a1-4db2-b99e-f92f654d9b8a.jpg!slider', '1967665a2c32b1039af5e6da4b8255e1', '0000-00-00 00:00:00', 0, 0),
(72, '苹果IBM联姻新动向：推出8款企业应用，覆盖医疗、零售、保险等领域', '未来结合Apple Watch更多场景的使用，苹果的用户绝不仅仅是果粉而已。', 'http://36kr.com/p/531388.html', 'http://d.36krcnd.com/nil_class/5aea57fd-10b3-4896-9c90-fc438dd7629e.png!slider', '906669949680f7ccf10e762f4b5acfb7', '0000-00-00 00:00:00', 0, 0),
(73, '所以说，乐视体育与飞鸽合作的自行车“超级”在了哪？', '价格和名字都还没公布，叫啥？你定！', 'http://36kr.com/p/531421.html', 'http://a.36krcnd.com/nil_class/c2e61f6f-debc-47ac-abb3-07da32391a5a.jpg!slider', 'aa1244614d4bbd8e58f6b541a8ee9dc7', '0000-00-00 00:00:00', 0, 0),
(74, '微信公众号顶部广告将支持App下载类推广，通过大数据分析精准推送', '变现、变现、变现……', 'http://36kr.com/p/531387.html', 'http://e.36krcnd.com/nil_class/7f5e1acd-39d3-42b8-aeeb-343ecd33ec31.jpg!slider', '7ee16b48be259849c637d5bd765f419a', '0000-00-00 00:00:00', 0, 0),
(75, '【氪TV视频】开着Rescue Lens，再不怕和客服鸡同鸭讲', '想提高好评率，用它就对了', 'http://36kr.com/p/531420.html', 'http://a.36krcnd.com/nil_class/def5b70b-92e6-43b6-a10d-c83f7b8aaec0.jpg!slider', '822eb231014af5d6b45824c8544aa386', '0000-00-00 00:00:00', 0, 0),
(76, '中国三明治推出个人价值分享平台“问达”，过去看故事，现在消费“人”', '也许过去是听过了很多故事，依然过不好这一生，现在或许已经不一样了。', 'http://36kr.com/p/531386.html', 'http://d.36krcnd.com/nil_class/4aa9fbd5-603d-4353-aaaf-f5ce4533f9c6.jpg!slider', 'a16e513de0a0bb6b3d589dea63f89ab3', '0000-00-00 00:00:00', 0, 0),
(77, '短距离即时物流“风先生”，为每位配送员买了个表', '当物流与智能硬件走到一起。', 'http://36kr.com/p/531419.html', 'http://b.36krcnd.com/nil_class/df623032-7246-4aea-b4a5-4ebd64c929c8.jpg!slider', 'de6b058e70fb68754dec966e39c48ce9', '0000-00-00 00:00:00', 0, 0),
(78, 'Sprinklr新一轮融资4600万美元，成为“独角兽”俱乐部新成员', '又一个一年估值翻番的传说', 'http://36kr.com/p/531384.html', 'http://a.36krcnd.com/nil_class/a14275fe-3a28-4ab6-ac26-642adbd7df1b.jpg!slider', '9f4d996a3ea573de836db6b1875646af', '0000-00-00 00:00:00', 0, 0),
(79, '苹果要确保Apple TV的用户体验，但是想让视频提供商付钱', '和Apple TV合作的视频CP将要自己承担视频数据传输的成本和责任。', 'http://36kr.com/p/531418.html', 'http://e.36krcnd.com/nil_class/617be8dd-f47d-4b78-a09c-c2ddd564169b.jpg!slider', '55b98aa976397dafdb4c214ec5732cdf', '0000-00-00 00:00:00', 0, 0),
(80, '微软推出 iOS 及 Android 平台智能助理应用“大眼夹”并将其开源', '不忘初心', 'http://36kr.com/p/531385.html', 'http://d.36krcnd.com/nil_class/ccaa44bc-4932-4087-b4e5-7fd19f7c693e.png!slider', 'a3de46630100179c2015328be8e09297', '0000-00-00 00:00:00', 0, 0),
(81, '瞄准电影、电视剧IP，阿里影业顶着亏损投资未来', 'BAT都在加大IP投入。', 'http://36kr.com/p/531417.html', 'http://a.36krcnd.com/nil_class/01aece10-6a5c-42b2-ab90-baba2b695cf6.jpg!slider', '21ff09173ddedf4b40fcf899002818fb', '0000-00-00 00:00:00', 0, 0),
(82, '困局当前的Pandora说，亦敌亦友的苹果没那么可怕', '数字音乐大年。', 'http://36kr.com/p/531383.html', 'http://c.36krcnd.com/nil_class/032d286c-c393-4645-bc27-32799b89c914.jpg!slider', 'e270c3d4727599bca9c6263d348653db', '0000-00-00 00:00:00', 0, 0),
(83, 'Believe it or not，新 MacBook 性能和 2011 款 MacBook Air 相当', '粉着见分', 'http://36kr.com/p/531416.html', 'http://c.36krcnd.com/nil_class/fc07276f-a867-4cac-8184-b724edf9c211.gif!slider', '550086ea5a4346103f37d4c0f5689c93', '0000-00-00 00:00:00', 0, 0),
(84, '脑洞大开的科技圈愚人节玩笑精选', '大开脑洞，人人有责。', 'http://36kr.com/p/531382.html', 'http://e.36krcnd.com/nil_class/36896fad-5cbe-4d96-89e0-b28aa9c65089.jpg!slider', '0993deafef96ec5c5ff984b898985c5e', '0000-00-00 00:00:00', 0, 0),
(85, '军用无人机：除了“鹰击长空”，还能“鱼翔浅底”，追踪抓捕潜水艇', '虽然长得丑了点，但还挺有用的', 'http://36kr.com/p/531413.html', 'http://e.36krcnd.com/nil_class/a70b2aba-89c7-4a88-b1ea-07438e323ed8.jpg!slider', '025a7cb52a63ae296174e32c789c0b47', '0000-00-00 00:00:00', 0, 0),
(86, '【氪TV视频】不写过程一样没分！解题还带过程的神器 Photomath', '我们真的不提倡作弊啊。', 'http://36kr.com/p/531381.html', 'http://e.36krcnd.com/nil_class/5ae1e47c-34e4-4af2-95c2-e6dedaf11aa3.png!slider', '9b5953bdd3197687aef4f788664bccd8', '0000-00-00 00:00:00', 0, 0),
(87, '左手教育，右手招聘，“猿圈”告诉你技术测评工具也有大前途', '别小瞧“测”，它要撬起了互联网创业的两个垂直领域：教育和招聘。', 'http://36kr.com/p/531415.html', 'http://a.36krcnd.com/nil_class/0e3e6792-097f-4da0-80b4-fb9f075196f4.png!slider', 'fdd684cb895f4b3cb1be01e9f2052509', '0000-00-00 00:00:00', 0, 0),
(88, '魔漫相机获2015年Facebook最佳应用奖，活跃用户多为25岁以上的女性用户', '依托Facebook发展是魔漫相机海外获奖的主要原因。', 'http://36kr.com/p/531414.html', 'http://b.36krcnd.com/nil_class/aeab5b0d-8109-4376-9fb7-a016cc319979.jpg!slider', 'b16073707c3768dfced7d6925d065edd', '0000-00-00 00:00:00', 0, 0),
(89, '现在起，可以在 Windows 和 OS X 电脑安装运行 Android 应用了', '看来，三个巨头的目标都一样', 'http://36kr.com/p/531412.html', 'http://d.36krcnd.com/nil_class/2f67a39a-1d4b-4dd0-914e-f8caf2a860bc.jpg!slider', 'cb88506c2939a6960e13657e1b8f0fc2', '0000-00-00 00:00:00', 0, 0),
(90, '医疗器械移动化，极简化，平价化，the New Sexy', '社区、家庭、前线，更多场景需要更专业的移动设备', 'http://36kr.com/p/531411.html', 'http://b.36krcnd.com/nil_class/750a08e8-d1b6-4322-acb5-998b12f27247.JPG!slider', '16684e11b47c6e2ca61bd39e80bf1988', '0000-00-00 00:00:00', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ranker_content`
--
ALTER TABLE `ranker_content`
  ADD PRIMARY KEY (`content_id`), ADD UNIQUE KEY `content_md5` (`content_md5`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ranker_content`
--
ALTER TABLE `ranker_content`
  MODIFY `content_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=91;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

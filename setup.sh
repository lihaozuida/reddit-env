# start mysql docker background
echo "init mysql"
docker rm -f db
cp /root/reddit-env/db/data.sql /tmp 
docker run -d -p 3307:3306 -v /tmp:/tmp --name db -e MYSQL_PASS="mypass" -e STARTUP_SQL="/tmp/data.sql" tutum/mysql
sleep 10
echo "mysql inited"

# build web docker
echo "start build web docker"
cd /root/reddit-env/app
docker build -t lihaozuida/reddit:latest .
echo "web docker ready"

# start app
echo "start app"
docker rm -f web
docker run -d -p 80:8080 --name web --link db:db lihaozuida/reddit
echo "app started"
